import { IProvider } from './api'
import axios from 'axios'

export default class AxiosProvider extends IProvider {
    constructor(baseURL) {
        super()
        this.baseURL = baseURL
        this._client = axios.create({
            baseURL,
            timeout: 1000
        })
    }

    getClient() {
        return this._client
    }

    setAuthHeader(token) {
        this._client.defaults.headers.common['Authorization'] = `Bearer ${token}`
    }

    clearAuthHeader() {
        this._client.defaults.headers.common['Authorization'] = ''
    }
}

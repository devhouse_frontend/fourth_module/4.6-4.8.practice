import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { APIService } from './api/api'
import AxiosProvider from './api/axios.provider'

const axiosProvider = new AxiosProvider('http://localhost:1337')

Vue.prototype.$api = new APIService(axiosProvider)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
